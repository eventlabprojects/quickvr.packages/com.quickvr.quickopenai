using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace QuickVR.AI
{

    public static class QuickAISettingsEditor
    {
        #region CONSTANTS

        private static string PATH = "Assets/QuickVRCfg/Resources/QuickAISettings.asset";

        #endregion

        #region GET AND SET

        public static void CheckFile()
        {
            var settings = AssetDatabase.LoadAssetAtPath<QuickAISettings>(PATH);
            if (!settings)
            {
                settings = ScriptableObject.CreateInstance<QuickAISettings>();
                QuickUtilsEditor.CreateDataFolder("QuickVRCfg/Resources");
                AssetDatabase.CreateAsset(settings, PATH);
                AssetDatabase.SaveAssets();
            }
        }

        #endregion

        class QuickXRSettingsPostprocessor : AssetPostprocessor
        {
            static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
            {
                CheckFile();
            }

        }
    }

}


