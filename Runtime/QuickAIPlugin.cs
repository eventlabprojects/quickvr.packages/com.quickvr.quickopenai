using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

using UnityEngine;

using OpenAI;
using ElevenLabs;
using ElevenLabs.Voices;

namespace QuickVR.AI
{

    public static class OpenAIModels
    {

        public static readonly string[] Chat =
        {
            "gpt-4o",
            "gpt-4o-mini",
            "gpt-4o-realtime-preview",
            "gpt-4o-mini-realtime-preview",
        };

        public static string[] Reasoning =
        {
            "o1",
            "o1-mini",
            "o3-mini",
        };

        public static string[] Transcription =
        {
            "whisper-1"
        };

    }

    /// <summary>
    /// Class used to communicate Unity with a specific AI Engine (ChatGPT, ElevenLabs,...)
    /// </summary>
    public static class QuickAIPlugin
    {

        public static string[] GPTModels =
        {
            //Chat 
            
            "o3-mini",
        };

        public static bool _isProcessingRequest
        {
            get; private set;
        } = false;

        public static bool _isRequestSuccess
        {
            get; private set;
        } = false;

        #region CONSTANTS

        public const string DEFAULT_MODEL_GPT = "gpt-4o";

        #endregion

        #region PRIVATE ATTRIBUTES

        //--> OpenAI ATTRIBUTES START <--
        /// <summary>
        /// A reference to the OpenAI API
        /// </summary>
        public static OpenAIApi _apiOpenAI
        {
            get
            {
                if (m_APIOpenAI == null)
                {
                    m_APIOpenAI = new OpenAIApi(QuickAISettings._instance._apiKey, QuickAISettings._instance._organization);
                }

                return m_APIOpenAI;
            }
        }
        private static OpenAIApi m_APIOpenAI = null;

        //--> OpenAI ATTRIBUTES END <--

        //--> ElevenLabs ATTRIBUTES START <--
        /// <summary>
        /// A reference to the ElevenLabs API
        /// </summary>
        private static ElevenLabsClient _apiElevenLabs = new ElevenLabsClient();

        /// <summary>
        /// Internal attribute used durin call to _apiElevenLabs
        /// </summary>
        private static CancellationTokenSource _lifetimeCancellationTokenSource = new CancellationTokenSource();
        //--> ElevenLabs ATTRIBUTES END <--

        #endregion

        #region CONSTANTS

        private const string WARNING_PROMPT_NULL_OR_EMPTY = "Request cannot be processed because 'prompt' is null or empty";

        #endregion

        #region GET AND SET

        /// <summary>
        /// Send a prompt to ChatGPT using an AudioClip. The answer from ChatGPT will be transformed to audio using the aiVoice. 
        /// </summary>
        /// <param name="prompt">The AudioClip containing the prompt. </param>
        /// <param name="aiVoice">The voice that will be used to transform ChatGPT's answer from TTS</param>
        /// <returns>The AucioClip which is the result of applying TTS to ChatGPT's answer. </returns>
        public static async Task<AudioClip> RequestChatGPT(AudioClip prompt, string model, Voice aiVoice = null)
        {
            AudioClip result = null;

            if (prompt != null)
            {
                string t = await SpeechToText(prompt);
                result = await RequestChatGPT(t, model, aiVoice);
            }
            else
            {
                Debug.LogWarning(WARNING_PROMPT_NULL_OR_EMPTY);
            }

            return result;
        }

        /// <summary>
        /// Send a prompt to ChatGPT using text. The answer from ChatGPT will be transformed to audio using the aiVoice. 
        /// </summary>
        /// <param name="prompt">The text containing the prompt. </param>
        /// <param name="aiVoice">The voice that will be used to transform ChatGPT's answer from TTS</param>
        /// <returns>The AucioClip which is the result of applying TTS to ChatGPT's answer. </returns>
        public static async Task<AudioClip> RequestChatGPT(string prompt, string model, Voice aiVoice = null)
        {
            AudioClip result = null;

            if (prompt != null && prompt.Length > 0)
            {
                Debug.Log("Q: " + prompt);
                string answer = await RequestChatGPT(prompt, model);
                Debug.Log("A: " + answer);
                result = await TextToSpeech(answer, aiVoice);
            }
            else
            {
                Debug.LogWarning(WARNING_PROMPT_NULL_OR_EMPTY);
            }

            return result;
        }

        /// <summary>
        /// Send a prompt to ChatGPT using text. The answer from ChatGPT will be also in the form of a text. 
        /// </summary>
        /// <param name="prompt">The text containing the prompt. </param>
        /// <returns>ChatGPT's answer. </returns>
        public static async Task<string> RequestChatGPT(string prompt, string model)
        {
            _isRequestSuccess = false;
            if (_isProcessingRequest)
            {
                Debug.LogError("Request cannot be processed because a previous request is still processing. ");
                return string.Empty;
            }

            if (string.IsNullOrEmpty(prompt))
            {
                Debug.LogError("Request cannot be processed because prompt is null or empty. ");
                return string.Empty;
            }

            //We can proceed to process the request. 
            _isProcessingRequest = true;
            string result = string.Empty;
            
            ChatMessage newMessage = new ChatMessage();
            newMessage.Content = prompt;
            newMessage.Role = "user";

            List<ChatMessage> messages = new List<ChatMessage>
            {
                newMessage
            };

            CreateChatCompletionRequest request = new CreateChatCompletionRequest();
            request.Messages = messages;
            request.Model = model;

            Debug.Log("REQUEST CHAT GPT START");
            Debug.Log($"model = {model}");
            var response = await _apiOpenAI.CreateChatCompletion(request);
            Debug.Log("REQUEST CHAT GPT END");

            if (response.Error == null)
            {
                if (response.Choices != null && response.Choices.Count > 0)
                {
                    var chatResponse = response.Choices[0].Message;

                    result = chatResponse.Content;
                }
            }
            else
            {
                Debug.LogError($"RequestChatGPT failed: {response.Error.Message}");                     
            }

            _isRequestSuccess = response.Error == null;
            _isProcessingRequest = false;

            return result;
        }

        /// <summary>
        /// Transforms an AudioClip to text using the specified parameters.
        /// </summary>
        /// <param name="aClip">The input AudioClip to be processed.</param>
        /// <param name="localeId">The language to be used for transcription. Defaults to "en".</param>
        /// <param name="modelName">The model to be used for transcription. Defaults to "whisper-1".</param>
        /// <param name="temperature">The temperature setting for the transcription. Defaults to 0 (optional).</param>
        /// <param name="prompt">An optional text prompt to guide the transcription.</param>
        /// <returns>The resulting text.</returns>
        //public static async Task<string> SpeechToText(
        //    AudioClip aClip,
        //    string localeId = "en",
        //    string modelName = "whisper-1",
        //    float temperature = 0,
        //    string prompt = "")
        //{
        //    // Convert the AudioClip to a WAV byte array
        //    byte[] data = SaveWav.Save("output.wav", aClip);

        //    // Create the transcription request
        //    var req = new CreateAudioTranscriptionsRequest
        //    {
        //        FileData = new FileData() { Data = data, Name = "audio.wav" },
        //        Model = modelName,
        //        Language = localeId,
        //        Temperature = temperature,
        //        Prompt = prompt
        //    };

        //    // Send the request to OpenAI API
        //    var res = await _apiOpenAI.CreateAudioTranscription(req);

        //    return res.Text;
        //}

        public static async Task<string> SpeechToText(AudioClip aClip)
        {
            //byte[] data = SaveWav.Save("output.wav", aClip);
            byte[] data = SavWav.GetWav(aClip, out uint lenght);

            var req = new CreateAudioTranscriptionsRequest
            {
                FileData = new FileData() { Data = data, Name = "audio.wav" },
                // File = Application.persistentDataPath + "/" + fileName,
                Model = "whisper-1",
                Language = "en"
            };

            var res = await _apiOpenAI.CreateAudioTranscription(req);

            return res.Text;
        }

        ///// <summary>
        ///// Transforms a text to an AudioClip using the Voice. 
        ///// </summary>
        ///// <param name="text">The text to be processed</param>
        ///// <param name="voice">The voice to be used on the TTS</param>
        ///// <returns>The resulting AudioClip</returns>
        //public static async Task<AudioClip> TextToSpeech(string text, Voice voice = null)
        //{
        //    if (voice == null)
        //    {
        //        voice = (await _apiElevenLabs.VoicesEndpoint.GetAllVoicesAsync(_lifetimeCancellationTokenSource.Token)).FirstOrDefault();
        //    }

        //    //Debug.Log("Voice: " + voice?.Name); // Log the voice being used

        //    var clipOffset = 0;
        //    //var streamCallbackSuccessful = false;
        //    var result = await _apiElevenLabs.TextToSpeechEndpoint.StreamTextToSpeechAsync(text, voice, audioClip =>
        //    {
        //        clipOffset = audioClip.samples;

        //        //if (clipOffset > 0)
        //        //{
        //        //    Debug.Log($"Stream Playback {clipOffset}");
        //        //    streamCallbackSuccessful = true;
        //        //    audioSource.PlayOneShot(audioClip);
        //        //}
        //    }, cancellationToken: _lifetimeCancellationTokenSource.Token);

        //    AudioClip clip = result.AudioClip;
        //    clip.LoadAudioData();

        //    return clip;
        //}

        public static async Task<AudioClip> TextToSpeech(string text, Voice voice = null)
        {
            if (voice == null)
            {
                voice = (await _apiElevenLabs.VoicesEndpoint.GetAllVoicesAsync(_lifetimeCancellationTokenSource.Token)).FirstOrDefault();
            }

            List<float> audioDataList = new List<float>(); // Store streamed audio data

            var result = await _apiElevenLabs.TextToSpeechEndpoint.StreamTextToSpeechAsync(text, voice, audioClip =>
            {
                if (audioClip != null)
                {
                    float[] chunkData = new float[audioClip.samples * audioClip.channels];
                    audioClip.GetData(chunkData, 0); // Get the chunk's raw data
                    audioDataList.AddRange(chunkData); // Append it to the list
                }
            }, cancellationToken: _lifetimeCancellationTokenSource.Token);

            if (audioDataList.Count == 0)
            {
                Debug.LogError("No audio data received from streaming.");
                return null;
            }

            // Convert list to array
            float[] finalAudioData = audioDataList.ToArray();

            // Create a new AudioClip with the same properties but **not streaming**
            AudioClip finalClip = AudioClip.Create("TTS_Audio", finalAudioData.Length, result.AudioClip.channels, result.AudioClip.frequency, false);
            finalClip.SetData(finalAudioData, 0); // Set the captured data

            return finalClip;
        }

        /// <summary>
        /// Selects the voice with the ID given 
        /// </summary>
        /// <param name="text">The text to be processed</param>
        /// <param name="voice">The voice to be used on the TTS</param>
        /// <returns>The resulting AudioClip</returns>
        public static async Task<Voice> SelectVoice(string id)

        {
            Voice voice = await _apiElevenLabs.VoicesEndpoint.GetVoiceAsync(id);

            return voice;
            
        }


        #endregion

    }

}


