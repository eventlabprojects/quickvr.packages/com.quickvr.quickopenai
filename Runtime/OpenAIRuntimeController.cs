using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;

using ElevenLabs.Voices;

namespace QuickVR.AI
{

    public class OpenAIRuntimeController : QuickAIConversationManager
    {

        #region PUBLIC ATTRIBUTES

        public QuickRuntimeAnimatorController _runtimeAnimatorController = null;

        public float _lookAtSwapTimeMin = 5.0f;
        public float _lookAtSwapTimeMax = 8.0f;

        /// <summary>
        /// The group will be considered in silence if none of the participants has talked on this last ammount of seconds. 
        /// </summary>
        public float _groupSilenceThreshold = 2.0f;

        public float _transitionTime = 0.5f;
        public bool _isThinking
        {
            get
            {
                return m_IsThinking;
            }
            protected set
            {
                if (value != m_IsThinking)
                {
                    _elapsedTime = 0;
                    m_IsThinking = value;
                }
            }
        }
        protected bool m_IsThinking = false;

        protected bool _isPlayingAnswer = false;

        public Voice _voice;

        #endregion

        #region PROTECTED ATTRIBUTES

        protected Animator _animator = null;
        protected AudioSource _audioSource = null;
        protected QuickAnimatorController _animatorController = null;
        protected QuickIKManager _ikManager = null;
        protected QuickLookAtManager _lookAtManager = null;

        [System.Serializable]
        protected class ParticipantData
        {
            public Transform _lookAtTransform = null;
            public QuickAIInput _aiInput = null;
            public QuickAudioAnalyzer _audioAnalyzer = null;

            public float _timeLastSpeaking
            {
                get
                {
                    return _audioAnalyzer == null ? -1 : _audioAnalyzer._timeLastSpeaking;
                }
            }

            public bool _isSpeaking
            {
                get
                {
                    return _audioAnalyzer == null? false : _audioAnalyzer._isSpeaking;
                }
            }
        }
        protected Dictionary<string, ParticipantData> _participantData = new Dictionary<string, ParticipantData>();
        protected ParticipantData _lastTalkingParticipant = null;

        protected float _elapsedTime = 0;

        protected AudioClip[] _responseAudios = null;

        protected enum State
        {
            Listening, 
            Answering,
        }
        protected State _state
        {
            get
            {
                return m_State;
            }
            set
            {
                if (value == State.Listening)
                {
                    _lastTalkingParticipant = null;
                    _lookAtManager.LookAtDefault();
                }
                else if (value == State.Answering)
                {
                    _elapsedTimeStateAnswering = 0;
                    _waitTimeLookAtSwap = Random.Range(_lookAtSwapTimeMin, _lookAtSwapTimeMax);

                    //Start looking at the AI trigger
                    var pData = GetTriggerParticipant();
                    if (pData == null)
                    {
                        _lookAtManager.LookAtDefault();
                    }
                    else
                    {
                        _lookAtManager.LookAtTransform(pData._lookAtTransform);
                    }
                }

                m_State = value;
            }
        }
        protected State m_State = State.Listening;

        /// <summary>
        /// The time passed since the avatar has select a target to look at. 
        /// </summary>
        protected float _elapsedTimeStateAnswering = 0;
        protected float _waitTimeLookAtSwap = 0;

        #endregion

        #region CONSTANTS

        protected const int LAYER_ID_THINKING = 1;

        #endregion

        #region EVENTS

        public UnityEvent<AudioClip> OnPlayAudio = new UnityEvent<AudioClip>();

        #endregion

        #region CREATION AND DESTRUCTION

        protected virtual void Awake()
        {
            _animator = GetComponent<Animator>();
            _animator.SetLayerWeight(LAYER_ID_THINKING, 0);
            _elapsedTime = _transitionTime;

            _animatorController = gameObject.GetOrCreateComponent<QuickAnimatorController>();
            _animatorController._runtimeAnimatorController = _runtimeAnimatorController;

            _ikManager = gameObject.GetOrCreateComponent<QuickIKManager>();
            _lookAtManager = gameObject.GetOrCreateComponent<QuickLookAtManager>();
        }

        protected override void Start()
        {
            base.Start();

            _audioSource = GetComponentInChildren<AudioSource>();
            if (!_audioSource)
            {
                _audioSource = gameObject.AddComponent<AudioSource>();
            }

            var ikSolverHead = _ikManager.GetIKSolver(IKBone.Head);
            ikSolverHead._weightIKPos = 0;
            ikSolverHead._weightIKRot = 1;
        }

        protected virtual void OnEnable()
        {
            QuickIKManagerUpdater.OnPreIKPass.AddListener(UpdateIKTargets);
        }

        protected virtual void OnDisable()
        {
            if (_animatorController != null)
            {
                QuickIKManagerUpdater.OnPreIKPass.RemoveListener(UpdateIKTargets);
            }
        }

        #endregion

        #region GET AND SET

        protected virtual ParticipantData GetTriggerParticipant()
        {
            foreach (var pair in _participantData)
            {
                if (pair.Value._aiInput._isAssistantTrigger)
                {
                    return pair.Value;
                }
            }

            return null;
        }

        public override void AddParticipant(string name, GameObject participant)
        {
            base.AddParticipant(name, participant);

            if (participant != null && participant != gameObject)
            {
                var pData = new ParticipantData();

                //Try to retrieve the lookAtTransform
                var animatorOther = participant.GetComponent<Animator>();
                if (animatorOther != null)
                {
                    pData._lookAtTransform = animatorOther.GetBoneTransform(QuickHumanBodyBones.EyeCenter);
                }

                //Try to retrieve the AudioAnalyzer
                pData._aiInput = participant.GetComponentInChildren<QuickAIInput>();
                if (pData._aiInput != null)
                {
                    pData._audioAnalyzer = pData._aiInput._audioAnalizer;
                }

                _participantData.Add(name, pData);
            }
        }

        public override void RemoveParticipant(string name)
        {
            base.RemoveParticipant(name);

            _participantData.Remove(name);
        }

        /// <summary>
        /// Returns the data of the participant that talked the last. 
        /// </summary>
        /// <returns></returns>
        protected virtual (string, ParticipantData) GetLastTalkingParticipant()
        {
            string key = string.Empty;
            ParticipantData pData = null;
            foreach (var pair in _participantData)
            {
                float t = pair.Value._timeLastSpeaking;
                if (pData == null || (t >= 0 && t > pData._timeLastSpeaking))
                {
                    key = pair.Key;
                    pData = pair.Value;
                }
            }

            return (key, pData);
        }

        protected virtual bool LookAtParticipant(string participantName)
        {
            bool result = LookAtParticipant(_participantData[participantName]);
            if (result)
            {
                Debug.Log($"looking at {participantName}");
            }
            else
            {
                Debug.LogWarning($"No look at target defined for participant {participantName}");
            }

            return result;
        }

        protected virtual bool LookAtParticipant(ParticipantData pData)
        {
            if (pData._lookAtTransform != null)
            {
                _lookAtManager.LookAtTransform(pData._lookAtTransform);
                return true;
            }

            return false;
        }

        #endregion

        #region UPDATE

        protected virtual void UpdateIKTargets()
        {
            //Let the IKTargets follow the animation
            for (IKBone ikBone = 0; ikBone < IKBone.LastBone; ikBone++)
            {
                Transform tBone = _animator.GetBoneTransform(QuickIKManager.ToHumanBodyBones(ikBone));
                var ikSolver = _ikManager.GetIKSolver(ikBone);
                ikSolver._targetLimb.position = tBone.position;

                //Avoid updating head rotation, as it will be driven by the LookAtManager
                if (ikBone != IKBone.Head)
                {
                    ikSolver._targetLimb.GetChild(0).rotation = tBone.rotation;
                }
            }
        }

        protected override IEnumerator CoProcessResponse(string response)
        {
            // Regular expression pattern to match . or ? or ! or ..., but removing the ! when splitting and keeping the other ones. 
            //This is because the text to speech conversion have "problems" (sound ike a robot sometimes) when an exclamation point is int he sentence
            string pattern = @"(?<=\.{3}|[.?])\s+(?!(?<=\.{3})\s)|(?<!\.)!";

            string[] sentences = Regex.Split(response, pattern)
                                      .Select(sentence => sentence.Trim()) // Trim each sentence
                                      .Where(sentence => !string.IsNullOrWhiteSpace(sentence)) // Exclude empty entries
                                      .ToArray();

            //Debug.Log(response);
            //Debug.Log($"numSentences = {sentences.Length}");
            _responseAudios = new AudioClip[sentences.Length];

            StartCoroutine(CoPlayResponse());

            for (int i = 0; i < sentences.Length; i++)
            {
                //Debug.Log(sentences[i]);
                var op = new WaitForTask<AudioClip>(QuickAIPlugin.TextToSpeech(sentences[i], _voice));
                yield return op;

                if (op._success)
                {
                    _responseAudios[i] = op._result;
                }
                else
                {
                    Debug.LogError($"TTS request failed: {op._error}");
                    yield break;
                }
                
                //Debug.Log(_responseAudios[i] == null);
            }
        }

        protected virtual IEnumerator CoPlayResponse()
        {
            _state = State.Answering;

            for (int i = 0; i < _responseAudios.Length; i++)
            {
                //Wait for the audio to be processed
                while (_responseAudios[i] == null)
                {
                    yield return null;
                }

                //Play the audio and wait till is finished. 
                PlayAudio(_responseAudios[i]);
                while (_audioSource.isPlaying)
                {
                    yield return null;
                }
            }

            //We have finished playing the answer
            _state = State.Listening;
        }

        protected virtual void PlayAudio(AudioClip audio)
        {
            if (_audioSource)
            {
                _audioSource.clip = audio;
                _audioSource.Play();
            }

            OnPlayAudio?.Invoke(audio);
        }

        protected virtual void Update()
        {
            //if (InputManagerVR.GetButtonDown(InputManagerVR.ButtonCodes.RightPrimaryPress))
            //{
            //    TestRequest();
            //}

            if (_elapsedTime < _transitionTime)
            {
                _elapsedTime += Time.deltaTime;

                float startWeight = _isThinking ? 0 : 1;
                float endWeight = _isThinking ? 1 : 0;
                float t = _elapsedTime / _transitionTime;
                //_animator.SetLayerWeight(LAYER_ID_THINKING, Mathf.Lerp(startWeight, endWeight, t));
            }

            _isPlayingAnswer = _audioSource && _audioSource.time != 0;

            if (_state == State.Listening)
            {
                UpdateLookAtStateListening();
            }
            else if (_state == State.Answering)
            {
                UpdateLookAtStateAnswering();
            }
        }

        protected virtual void UpdateLookAtStateListening()
        {
            //Try to look at the last participant that is talking. 
            if (_lastTalkingParticipant == null || !_lastTalkingParticipant._isSpeaking)
            {
                (string key, ParticipantData pData) = GetLastTalkingParticipant();
                _lastTalkingParticipant = pData;
            }

            if (_lastTalkingParticipant == null || (Time.time - _lastTalkingParticipant._timeLastSpeaking) > _groupSilenceThreshold)
            {
                //No one is talking right now
                _lookAtManager.LookAtDefault();
            }
            else
            {
                //Look at the player that talked the last. 
                LookAtParticipant(_lastTalkingParticipant);
            }
        }

        protected virtual void UpdateLookAtStateAnswering()
        {
            //Randomly look at the participants in the conversation while is playing its response. 
            _elapsedTimeStateAnswering += Time.deltaTime;
            if (_elapsedTimeStateAnswering >= _waitTimeLookAtSwap)
            {
                _elapsedTimeStateAnswering = 0;
                _waitTimeLookAtSwap = Random.Range(_lookAtSwapTimeMin, _lookAtSwapTimeMax);

                //Select a random participant to look at
                int numTransforms = _participantData.Count;
                if (numTransforms > 0)
                {
                    string randomKey = _participantData.Keys.ElementAt(Random.Range(0, numTransforms));
                    LookAtParticipant(randomKey);
                }
            }
        }

        #endregion

        #region DEBUG

        [ButtonMethod]
        public virtual void Test()
        {
            _isThinking = !_isThinking;
        }

        #endregion

    }

}
