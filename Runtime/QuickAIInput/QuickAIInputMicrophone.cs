using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuickVR.AI
{

    public class QuickAIInputMicrophone : QuickAIInput
    {

        #region CREATION AND DESTRUCTION

        protected virtual void Awake()
        {
            _audioAnalizer = QuickSingletonManager.GetInstance<QuickMicrophone>();
        }

        #endregion

    }

}
