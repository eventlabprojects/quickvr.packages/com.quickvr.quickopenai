using UnityEngine;

namespace QuickVR.AI
{

    public abstract class QuickAIInput : MonoBehaviour
    {

        #region PUBLIC ATTRIBUTES

        public QuickAIConversationManager _conversationManager = null;
        public string _speakerName = string.Empty;
        public bool _isAssistantTrigger = false;

        public QuickAudioAnalyzer _audioAnalizer
        {
            get; protected set;
        }

        #endregion

        #region CREATION AND DESTRUCTION

        protected virtual void OnEnable()
        {
            if (_audioAnalizer != null)
            {
                _audioAnalizer.OnSpeakingStart.AddListener(AudioRecordStart);
                _audioAnalizer.OnSpeakingEnd.AddListener(AudioRecordStop);
            }
        }

        protected virtual void OnDisable()
        {
            if (_audioAnalizer != null)
            {
                _audioAnalizer.OnSpeakingStart.RemoveListener(AudioRecordStart);
                _audioAnalizer.OnSpeakingEnd.RemoveListener(AudioRecordStop);
            }
        }

        protected virtual void Start()
        {
            if (_speakerName == string.Empty)
            {
                _speakerName = name;
            }

            if (_conversationManager == null)
            {
                _conversationManager = FindObjectOfType<QuickAIConversationManager>();
                if (_conversationManager != null)
                {
                    _conversationManager.AddParticipant(_speakerName, gameObject);
                }
            }
        }

        #endregion

        #region GET AND SET

        protected virtual void AudioRecordStart()
        {
            _audioAnalizer.StartRecording();
        }

        protected virtual void AudioRecordStop()
        {
            ProcessPrompt(_audioAnalizer.StopRecording());
        }

        #endregion

        #region UPDATE

        protected virtual async void ProcessPrompt(AudioClip aClip)
        {
            string prompt = string.Empty;
            if (aClip != null)
            {
                //string timeStamp = System.DateTime.Now.ToString("yyyyMMdd_HHmmss");
                //_audioAnalizer.SaveLastRecording($"RecordedSpeech_{timeStamp}.wav");
                Debug.Log("Transcribing...");
                prompt = await QuickAIPlugin.SpeechToText(aClip);
                Debug.Log($"Transcription: {prompt}");
            }

            if (_conversationManager != null)
            {
                _conversationManager.AddStatement(_speakerName, prompt, _isAssistantTrigger);
            }
        }

        #endregion

    }

}
