using UnityEngine;

namespace QuickVR.AI
{

    public class QuickAIInputAudioSource : QuickAIInput
    {

        #region CREATION AND DESTRUCTION

        protected virtual void Awake()
        {
            var aSource = GetComponentInChildren<AudioSource>();
            if (aSource != null)
            {
                _audioAnalizer = aSource.GetOrCreateComponent<QuickAudioSource>();
            }
            else
            {
                Debug.LogError($"{GetType().Name} requires an AudioSource on this object or any child in order to work. ");
            }
        }

        #endregion

    }

}
