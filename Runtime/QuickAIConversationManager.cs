using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

namespace QuickVR.AI
{

    [System.Serializable]
    public class PromptHolder
    {
        [LanguageCode]
        public string _languageID;

        [LargeTextArea]
        public string _text;
    }

    public class QuickAIConversationManager : MonoBehaviour 
    {

        #region PUBLIC ATTRIBUTES

        public string _aiAssistantName = "AI Assistant";

        [StringDropdown(typeof(OpenAIModels), "Chat")]
        public string _assistantModel = OpenAIModels.Chat[0];

        [StringDropdown(typeof(OpenAIModels), "Chat")]
        public string _summaryModel = OpenAIModels.Chat[0];

        public int _maxRecentStatements = 10;   //5;  // How many raw statements to keep before summarizing
        public int _maxSummaryLength = 1000;    //500;   // Maximum characters for the rolling summary

        [LanguageCode]
        public string _currentLanguageID = "en";

        public List<PromptHolder> _initialPrompt = new List<PromptHolder>();

        /// <summary>
        /// The AI assistant will only answer if it finds any of the keywords in the prompt. 
        /// </summary>
        public List<string> _keywords = new List<string>();

        [Header("Debug")]
        /// <summary>
        /// A series of predefined requests to try the system. 
        /// </summary>
        public string[] _testRequests = { "Hi, how are you doing?" };

        /// <summary>
        /// The index of the request being used when testing. 
        /// </summary>
        public int _testRequestID = 0;

        #endregion

        #region PROTECTED ATTRIBUTES

        protected List<string> _recentStatements = new List<string>();
        protected string _rollingSummary = "";  // Short summary of the discussion

        protected int _initialPromptIndex
        {
            get
            {
                if (m_InitialPromptIndex == -1)
                {
                    for (int i = 0; i < _initialPrompt.Count; i++)
                    {
                        var p = _initialPrompt[i];
                        if (p._languageID == _currentLanguageID)
                        {
                            m_InitialPromptIndex = i;
                            break;
                        }
                    }
                }

                return m_InitialPromptIndex;
            }
        }
        protected int m_InitialPromptIndex = -1;

        protected bool _isProcessing = false;

        /// <summary>
        /// The participants in the conversation, including the AI agent. 
        /// </summary>
        protected Dictionary<string, GameObject> _participants = new Dictionary<string, GameObject>();

        #endregion

        #region CREATION AND DESTRUCTION

        protected virtual void Start()
        {
            AddParticipant(_aiAssistantName, gameObject);
        }

        #endregion

        #region GET AND SET

        public virtual void AddParticipant(string name,  GameObject participant)
        {
            _participants[name] = participant;
        }

        public virtual void RemoveParticipant(string name)
        {
            _participants.Remove(name); 
        }

        public virtual void AddStatement(string speaker, string statement, bool isAssistantTrigger)
        {
            string formattedStatement = $"{speaker}: {statement}";

            // Store recent statements
            _recentStatements.Add(formattedStatement);

            // If too many recent statements, summarize
            if (_recentStatements.Count > _maxRecentStatements)
            {
                SummarizeConversation();
            }

            Debug.Log($"isAssistantTrigger = {isAssistantTrigger}");

            // Trigger the assistant
            if (isAssistantTrigger && IsKeywordFound(statement))
            {
                StartCoroutine(CoProcessConversation());
            }
        }

        protected virtual bool IsKeywordFound(string statement)
        {
            if (_keywords == null || _keywords.Count == 0)
            {
                return true;
            }

            var (matchedKeyword, bestPosition, bestMatchPercentage) = StringMatching.FindBestKeyword(statement, _keywords);
            return bestMatchPercentage >= 80;
        }

        public virtual string GetCurrentPrompt()
        {
            string initialPrompt = _initialPromptIndex >= 0 ? _initialPrompt[_initialPromptIndex]._text : null;
            //return $"{initialPrompt} \n Summary: {_rollingSummary}\nRecent: {string.Join("\n", _recentStatements)}";
            return $"{initialPrompt}\n\n### Summary:\n{_rollingSummary}\n\n### Recent Statements:\n{string.Join("\n", _recentStatements)}\n\n### AI Response:";
        }

        #endregion

        #region UPDATE

        protected virtual async void SummarizeConversation()
        {
            // Combine recent statements
            string rawText = string.Join(" ", _recentStatements);

            // Call an AI model or use a heuristic approach to summarize
            string prompt = $"Summarize the following conversation using a maximum of {_maxSummaryLength} characters: \n {rawText}";
            _rollingSummary = await QuickAIPlugin.RequestChatGPT(prompt, _summaryModel);

            // Clear recent statements but keep the last one for continuity
            _recentStatements = _recentStatements.TakeLast(1).ToList();
        }

        protected virtual IEnumerator CoProcessConversation()
        {
            if (_isProcessing)
            {
                Debug.LogWarning("Conversation cannot be processed because a previous request is still processing. ");
                yield break;
            }

            _isProcessing = true;
            
            string prompt = GetCurrentPrompt();
            Debug.Log(prompt);
            var op = new WaitForTask<string>(QuickAIPlugin.RequestChatGPT(prompt, _assistantModel));
            yield return op;
            //Debug.Log(response);

            if (op._success)
            {
                string response = op._result;
                AddStatement(_aiAssistantName, response, false);
                yield return StartCoroutine(CoProcessResponse(response));
            }
            else
            {
                Debug.LogError($"ChatGPT request failed: {op._error}");
            }

            _isProcessing = false;
        }

        protected virtual IEnumerator CoProcessResponse(string response)
        {
            //Do any needed additional work with response here. 
            yield break;
        }

        #endregion

        #region DEBUG

        [ButtonMethod]
        public virtual void TestRequest()
        {
            if (_testRequests != null && _testRequestID >= 0 && _testRequestID < _testRequests.Length)
            {
                AddStatement("Moderator", _testRequests[_testRequestID], true);
            }
            else
            {
                Debug.LogError($"TestRequest could not be performed because _testRequests is null or _testRequestID is out of bounds. ");
            }
        }

        #endregion

    }


}


