using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuickVR.AI
{

    public class QuickAISettings : ScriptableObject
    {

        #region PUBLIC ATTRIBUTES

        public static QuickAISettings _instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = Resources.Load<QuickAISettings>("QuickAISettings");
                }

                return m_Instance;
            }
        }
        private static QuickAISettings m_Instance = null;

        [Header("OpenAI Settings")]
        public string _apiKey;
        public string _organization;

        #endregion

    }

}


